﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Speech.Recognition;
using SpeechRecognition;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.IO;
namespace Voice_Recognition
{
    class Program
    {
        
        static void Main(string[] args)
        {
            SpeechRecognitionEngine recEngine = new SpeechRecognitionEngine();
            recEngine.SetInputToDefaultAudioDevice();
            
            PreparedStatement stmt = new PreparedStatement(PreparedEventStatements.GRAMMAR_TEST);
            stmt.SetValue(0, 2);
            MySqlDataReader read = stmt.Execute();
            Console.WriteLine(read[0]);
            stmt.ReorientStatement(PreparedEventStatements.EVENT_CALCULATE);
            read = stmt.Execute();
            Console.WriteLine(read[0]);


            
            string python = @"C:\Users\Horia\Anaconda3\python.exe";
            ProcessStartInfo proc = new ProcessStartInfo(python);
            string myPythonApp = @"C:\Users\Horia\substring.py";
            proc.UseShellExecute = false;
            proc.RedirectStandardOutput = true;
            string testStr = null;
            for (int i = 0; i <7; i++)
            {
                testStr += " " + i.ToString();
            }
            proc.Arguments = myPythonApp + " " + testStr;

            Process myProcess = new Process();
            myProcess.StartInfo = proc;
            myProcess.Start();

            StreamReader reader = myProcess.StandardOutput;

            Console.WriteLine("\n" + reader.ReadLine());
            



            //LoadGrammar();
            DictationGrammar grammar = new DictationGrammar();

            recEngine.LoadGrammarAsync(grammar);
            recEngine.RecognizeAsync(RecognizeMode.Multiple);
            recEngine.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(engine_SpeechRecognized);
            recEngine.MaxAlternates = 3;
            Console.ReadLine();
        }

        static void engine_SpeechRecognized(object ob, SpeechRecognizedEventArgs e)
        {
            
            foreach(RecognizedPhrase phrase in e.Result.Alternates)
            {
                Console.WriteLine(phrase.Text + phrase.Confidence); // multiple choices ( 3 now )
            }
            
            
        }


        static void LoadGrammar()
        {
            Choices text = new Choices();
            String rawString = System.IO.File.ReadAllText(@"GrammarInfo.txt");
            grammarArray = rawString.Split(',');
            text.Add(grammarArray);
            GrammarBuilder gbuilder = new GrammarBuilder(text);
            grammar = new Grammar(gbuilder);
        }

        
        static String[] grammarArray;
        static Grammar grammar;
    }
}


/*
 * Example of SQL query using the prepared statements 
 * 
            BaseStatement baseStmt = new BaseStatement();
            PreparedStatement stmt = new PreparedStatement(0, baseStmt.GetStatement(PreparedEventStatements.EVENT_PLAY));

            stmt.SetValue(0, 1); -> this sets the values to be replaced in '?'
            MySqlDataReader read = stmt.Execute(); -> optput values, if it's insert output will be null

            Console.WriteLine(read[3]);
 */
