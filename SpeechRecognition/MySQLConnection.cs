﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Diagnostics;
using System.Runtime.InteropServices;
namespace SpeechRecognition
{
    class MySQLConnection
    {   
       // nothing here yet
    }

}

public class PreparedStatement : BaseStatement
{
    public PreparedStatement(PreparedEventStatements command)
    {

        baseStmt = new BaseStatement();
        _command = baseStmt.GetStatement(command);
        stmtVal = new List<Object>();
        
        newCommand = new StringBuilder(_command); // new command string to be edited
        // also add here how many '?' signs are in stmtVal
        maxIndex = _command.Count(c => c == '?'); 

        for(Int32 i = 0; i < maxIndex; i++)
        {
            stmtVal.Add(new Object());
        }

    }

    public void ReorientStatement(PreparedEventStatements command)
    {
        _command = baseStmt.GetStatement(command);

        stmtVal.Clear(); // empty the previous list

        // reinitialize the StringBuilder newCommand
        newCommand.Remove(0, newCommand.Length);
        newCommand.Insert(0, _command);

        maxIndex = _command.Count(c => c == '?');

        for (Int32 i = 0; i < maxIndex; i++)
        {
            stmtVal.Add(new Object());
        }
    }

    public void SetValue(Int32 index, Object value)
    {
        stmtVal[index] = value;
    }

    public MySqlDataReader Execute()
    {
        String connectInfo = "datasource=127.0.0.1;port=3306;username=Overlord;password=justsql;database=jarvis"; // connection to database info

        MySqlConnection conn = new MySqlConnection(connectInfo); // create a new connection
        MySqlCommand command = conn.CreateCommand();

        // here edit the command to add the arguments from stmtVal
        char[] array = _command.ToCharArray();
        Int32 counter = 0;
        for (Int32 i = 0; i < array.Length; i++)
        {
            if (array[i] == '?')
            {
                newCommand.Remove(i, 1);
                newCommand.Insert(i, stmtVal[counter]);
                counter++;
            }
        }

        command.CommandText = newCommand.ToString(); // Final Command to be executed


        
        // connection open
        conn.Open();
        MySqlData = command.ExecuteReader();
        MySqlData.Read();
        return MySqlData;
            
       
    }

    private MySqlDataReader MySqlData;
    private StringBuilder newCommand;
    private List<Object> stmtVal;
    private string _command;
    private Int32 maxIndex;
    private BaseStatement baseStmt;


}
public class BaseStatement
{
    public BaseStatement()
    {
        statement = new Dictionary<PreparedEventStatements, string>();
        PrepareStatement(PreparedEventStatements.EVENT_PLAY, "SELECT * FROM event WHERE entry = ?  ");
        PrepareStatement(PreparedEventStatements.EVENT_PAUSE, "SELECT * FROM event WHERE entry = 2 ");
        PrepareStatement(PreparedEventStatements.EVENT_NEXT, "SELECT ....");
        PrepareStatement(PreparedEventStatements.EVENT_PREVIOUS, "SELECT ....");

        PrepareStatement(PreparedEventStatements.EVENT_GOOGLE_WINDOW, "SELECT ....");

        PrepareStatement(PreparedEventStatements.EVENT_YOUTUBE_WINDOW, "SELECT ....");

        PrepareStatement(PreparedEventStatements.EVENT_VOLUMEN_UP, "SELECT ....");
        PrepareStatement(PreparedEventStatements.EVENT_VOLUME_DOWN, "SELECT ....");

        PrepareStatement(PreparedEventStatements.EVENT_CLOSE_WINDOW, "SELECT ....");
        PrepareStatement(PreparedEventStatements.EVENT_KEYPRESS_ENTER, "SELECT ....");
        PrepareStatement(PreparedEventStatements.EVENT_NEXT_NET, "SELECT ....");
        PrepareStatement(PreparedEventStatements.EVENT_PREVIOUS_NET, "SELECT ....");

        PrepareStatement(PreparedEventStatements.EVENT_PLOT_VALUES, "SELECT ....");
        PrepareStatement(PreparedEventStatements.EVENT_CALCULATE, "SELECT grammar FROM grammarBuilder where entry = 1 ");
        PrepareStatement(PreparedEventStatements.EVENT_RESUT, "SELECT ....");
        PrepareStatement(PreparedEventStatements.EVENT_DICTATION, "SELECT ....");
        PrepareStatement(PreparedEventStatements.GRAMMAR_TEST, "SELECT grammar FROM grammarBuilder where entry = ? ");
    }
    public string GetStatement(PreparedEventStatements index)
    {
        return statement[index];
    }
    private void PrepareStatement(PreparedEventStatements index, string command)
    {
        statement.Add(index, command);
    }
    
    private Dictionary<PreparedEventStatements, string> statement;
}

public enum PreparedEventStatements
{
    //Music Player
    EVENT_PLAY,
    EVENT_PAUSE,
    EVENT_NEXT,
    EVENT_PREVIOUS,

    //Google Search
    EVENT_GOOGLE_WINDOW,

    //Youtube Search
    EVENT_YOUTUBE_WINDOW,

    //General
    EVENT_VOLUMEN_UP,
    EVENT_VOLUME_DOWN,
    EVENT_CLOSE_WINDOW,
    EVENT_TYPE,
    EVENT_KEYPRESS_ENTER,
    EVENT_NEXT_NET,
    EVENT_PREVIOUS_NET,

    //Advanced
    EVENT_PLOT_VALUES,
    EVENT_CALCULATE,
    EVENT_RESUT,
    EVENT_DICTATION,

    //Grammar
    GRAMMAR_TEST,



    EVENT_MAX_SIZE
};